package br.com.proway.revisao;

import br.com.proway.revisao.models.Dados;
import br.com.proway.revisao.models.TV;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Dados dados = new Dados();
        int id = 1;
        String texto;

        while(true) {
            texto = "       Sistema de TVs\n";
            for (int i = 0; i < dados.listaTVs.size(); i++) {
                texto += dados.listaTVs.get(i).id + " Modelo: " + dados.listaTVs.get(i).modelo + " Polegadas: " +
                        dados.listaTVs.get(i).polegada + "\n";
            }

            String[] opcoes = {"Adicionar", "Remover", "Sair"};
            int opcao = JOptionPane.showOptionDialog(null, texto, "TV",
                    JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE,
                    null, opcoes, opcoes[0]);

            if (opcao == 0) {
                String modelo = JOptionPane.showInputDialog("Modelo: ");
                int polegadas = Integer.parseInt(JOptionPane.showInputDialog("Polegadas(em números): "));

                dados.inserirTV(new TV(id, modelo, polegadas));
                id++;
            }

            if (opcao == 1) {
                int idTV = Integer.parseInt(JOptionPane.showInputDialog("ID para deletar TV: "));
                dados.deletarTV(idTV);
            }

            if (opcao == 2) {
                break;
            }
        }
    }
}
