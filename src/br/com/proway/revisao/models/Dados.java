package br.com.proway.revisao.models;

import javax.swing.*;
import java.util.ArrayList;

public class Dados {
    public ArrayList<TV> listaTVs = new ArrayList<>();

    public void inserirTV(TV tv) {
        listaTVs.add(tv);
    }

    public void deletarTV(int id) {
        listaTVs.removeIf(t -> t.id == id);
    }
}
