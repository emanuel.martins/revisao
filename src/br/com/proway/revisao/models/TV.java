package br.com.proway.revisao.models;

public class TV {
    public int id;
    public String modelo;
    public int polegada;

    public TV(int id, String modelo, int polegada) {
        this.id = id;
        this.modelo = modelo;
        this.polegada = polegada;
    }
}
